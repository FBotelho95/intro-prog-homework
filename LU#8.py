#1-2-3

class Robot:
    
    def __init__(self):
        self.recordings=[]
    
    def listen (self, string):
        self.recordings.append(string)
    
    def play_recordings(self):
        print(self.recordings)
    
    def delete_recordings(self):
        self.recordings=[]
    
r1 = Robot()
r2 = Robot()


r1.listen('hello, world')
r1.play_recordings()

r2.listen('goodbye, world')
r2.play_recordings()

r1.delete_recordings()
r1.play_recordings()

#4
class LectureRoom:
    
    def __init__(self):
        self.capacity=40
    def increase_capacity(self,number):
        if self.capacity+number<=100:
            self.capacity=self.capacity+number
        elif self.capacity+number>=10:
            self.capacity=self.capacity+number
        else:
            print('Error, capacity not valid')
   
    def decrease_capacity (self,number):
        if self.capacity-number>=10:
            self.capacity=self.capacity-number
        elif:self.capacity-number<=100:
            self.capacity=self.capacity-number 
        else:
            print('Error, capacity not valid')
    
    def capacity_status(self):
        print(self.capacity)
        
    
LR = LectureRoom()

LR.decrease_capacity(1)

LR.capacity_status()

LR.increase_capacity(50)
LR.capacity_status()


LR.decrease_capacity(80)
LR.capacity_status()