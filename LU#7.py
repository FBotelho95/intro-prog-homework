#Exercise #1

bool (1) #True
bool (1.1) #True
bool (0) #False
bool (0.0001) #True
bool ('') #False
bool (None) #False


#Exercise #2

ticker = 'AAPL'

def ticker_symbol ():
    if ticker == 'AAPL':
        print (True)
    else:
        print (False)


#Exercise #3
        
gender = 'Male'

def gender_color():
    if gender == 'Male':
        print ('You hate pink')
    if gender == 'Female':
        print ('You like pink')
        
gender_color ()
        

#Exercise #4

def gender(gender):
    if gender == 'male':
        print ('You hate pink')
    elif gender == 'female':
        print ('You like pink')
    else:
        print ('This line shouldnt be printed')


#Exercise #5
        
def parents_age (mother, father):
    if mother == father :
        print (mother+father)
    else:
        print (mother-father)
        
        
#Exercise #6
        
a_list = [1,2,3,4]

a_dict = {
        'AAPL': 1,
        'TSLA': 2,
        'FB': 3
        }

def lenght (a,b):
    if len (a_list) == len (a_dict):
        print ('awesome')
    else:
        print ('that is sad')
        
lenght (a_list,a_dict)


#Exercise #7

stock_prices = {
        'AAPL': 10,
        'GOOG': 15,
        'FB': 20
        }

for key in stock_prices:
    print(key)

for key in stock_prices:
    print(stock_prices [key])
    
for key in stock_prices:
    print('The stock price of',key,'is',stock_prices[key])


#Exercise #8
    
tuple = (10,20,30)
list = [40,50,60]
dict = {
        'AAPL': 100,
        'GOOG': 90,
        'FB': 80
        }

for num in tuple:
    print (num)
    
for num in list:
    print (num)

for key in dict:
    print (dict[key])


#Exercise #9
 
def list_function (list):
    return [num*2 for num in list]    

one_list = [1,2,3,4]

list_function (one_list)
    

#Exercise #10

dict_10 = {
        'AAPL': 50,
        'GOOG': 100,
        'FB': 200
        }

def dict_function (dict):
    return {key:dict[key]*2 for key in dict}

dict_function (dict_10)


#Exercise #11
    
def lenght_exercise (list,number):
    if len (list) > number:
        return True
    else: 
        return False
        
lenght_exercise (list,3)  