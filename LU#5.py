# Exercise #1
    # Explain what scope is: scope is basically the visibility a variable can have. Depending on where it was assigned, 
    #the variable has different levels of usability
    # local scope: the scope of function is its local scope (within the machinery)
    # global scope: any variable defined in the body of the code editor (outside a function) is in the global scope.
    
# Exercise #2
    # identify scopes...
     
# Exercise #3
    # Function without return statement... With no usability. Nothing happens when we call such functions
    
# Exercise #4    
a = 4
def double ():
    ret_val = a*2
    return ret_val

b = double ()

print (a)
print (b)

# Exercise #5

power = 10
print (power)

def generate_power (number):
    _power_ = 2
    def nth_power ():
        return number**power
    powered = nth_power()
    return powered

print (_power_)  #removing this line will allow the code to run properly
a = generate_power (2)
print (a)

# Exercise #6

power = 10
print (power)

def generate_power (number):
    _power_ = 2
    def nth_power ():
        return number**power
    powered = nth_power()
    return powered

print (power)
a = generate_power (2)
print (a)
    # Power is defined in two different scopes! in the global and inside the function. When we print the varianble print 
    #in the global scope it will always reffer to the varaible assigned in that same scope. The power variable defined 
    #within the function will only be used inside the function, after the function we are back on the global scope.
    
# Exercise #7

_what = 'coins'
_for = 'chocolats'

def transaction (_what,_for):
    _what = 'oranges'
    _for = 'bananas'
    print (_what,_for)
    
transaction (_what,_for)
print (_what,_for)

# Exercise #9
def full_name (first_name = 'Francisco', last_name = 'Botelho'):
    print (first_name,last_name)
    
full_name () #It uses the default variables
full_name (last_name = 'Hanks') #It changes the rassigned variable!
full_name (José, Peseiro) #NameError, variable not defined... Doubt here - No key??

# Exercise #10

first_name = 'Ricardo'
last_name = 'Pereira'

def name (first_name = 'Sam',last_name = 'Hopkins'):
    print (first_name,last_name)
    
name ()                                                 #prints Sam Hopkins
name (first_name = first_name,last_name = last_name)    #prints Ricardo Pereira
name (first_name = 'Jose',last_name = 'Peseiro')        #prints Jose Peseiro

# Exercise #11


def exercise_11 (message = 'Good Morning',name = 'Ricardo'):
    print (message,name)

exercise_11()
exercise_11 (name = 'Jonh Snow')
exercise_11 (message = 'You know nothing')


