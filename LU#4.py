# Exercise #1

def power(a, b):
    retval = a**b
    return retval

print (power (2, 3))                        


# Exercise #2

def equality (a, b):
    retval = a==b
    return retval

print (equality(2, 2.0))

# Exercise #3

def hypotenuse (a, b):
    retval = (a**2+b**2)**0.5
    return retval
print (hypotenuse (8, 6))

# Exercise 4

bitcoin = 10
def bit_litecoin (n):
    retval = 113.91*n
    return retval

def bit_etherium (n):
    retval = 30.32*n
    return retval

def bit_euros (n):
    retval = 5441.69*n
    return retval

print (bitcoin,"bitcoins are equivalent to", bit_litecoin(bitcoin),"litecoins")
print (bitcoin,"bitcoins are equivalent to", bit_etherium(bitcoin),"etheriums")
print (bitcoin,"bitcoins are equivalent to", bit_euros(bitcoin),"euros")

# Exercise #5

def hello():
    """Print 'Hello World' and return None"""
    print ("Hello World")
    
hello()

# Exercise #6

type (hello())

# Exercise #7
    # Printing is for humans, returning is for programs
    # A function that only prints a value is simply showing the value, is not performing any calculations.
    # A fucntion taht returns something, may not even display the final result, but it is performing calculations
    
