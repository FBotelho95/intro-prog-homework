# Exericse #0

tuple = ('a','b','c')


# Exericse #1

# Try to chnage a value in a tuple is impossible, because tuples are not for
#changing values.


# Exericse #2

AAPL_prices_5years = (65.55,109.65,92.65,118.07,165.53)


# Exericse #3

# AttributeError: 'tuple' object has no attribute 'sort'


# Exericse #4

AAPL_prices = [65.55,109.65,92.65,118.07,165.53]
AAPL_prices.append(58.18)


# Exericse #5

# Tuples are a list of things that we dont need to change while lists are for 
#changing, to add, remove, re-order them.


# Exericse #6

#(1,2,3) == (1,2,3)
#Out[13]: True

#(1,2,3) == (1,3,2)
#Out[14]: False

#[1,2,3] == [1,2,3]
#Out[15]: True

#[1,2,3] == (1,2,3)
#Out[16]: False


# Exericse #7

my_tuple = (1,2,3,4)
list(my_tuple)
my_list = list(my_tuple)


# Exericse #8

stock_price = {'AAPL':1,'TSLA':2,'C':3,'MSFT':4}
stock_price.keys()
stock_price.values()
stock_price.items()


# Exericse #9

hello = {'hello':'world'}
hello ['hi'] = 'Portugal'


# Exericse #10

stock_prices = {'AAPL':100,'GOOG':99}
stock_of_interest = 'AAPL'

print (stock_prices[stock_of_interest])


# Exericse #11

my_tupple = (1,2,3)
or_tupple = tuple([1,2,3,4])

my_list = [1,2,3,4]
list((1,2,3,4))

tupppie = (1,2,3,4)
list(tupppie)

# Exericse #12

del stock_price['AAPL'] 

stock_price.pop ('MSFT')

key_to_remove = 'c'
d ={'a':1,'b':2,'c':3}
if key_to_remove in d:
    del d[key_to_remove]
    
    
# Exericse #13

list = ['hey','bro']

def list_append (list):
    ret_val = list.append('homie')
    return ret_val
   
list_append (list)
list

    
# Exericse #14

def average_stocks (a,b,c,d):
    return sum ([
            a,b,c,d
            ])/4
    
average_stocks (1,3,5,7)

(1,2,3) == (1,2,3)