#Exercise #1

stock = 'TSLA'
shares = 200
price = 298.7
action = 'sell'

print('I would like to',action,shares,'shares','in',stock)
print('The current',stock,'stock price is',price)

#Exercise #2

person1 = 'Francisco'
amount = 10
person2 = 'Pedro'
number = 2
ticker = 'AAPL'

print (person1,'needs to borrow',amount,'euros from',person2,'in order to trade',number,'stocks in',ticker)

#Exercise #3
x = 2
y = 4

x*y

z = x*y
print (z)

#Exercise #4

celsius = 30
fahrenheit = (9*celsius+160)/5
print (celsius,'celsius degress are equivalent to',fahrenheit,'fahrenheit degrees')

kelvin = celsius+273.15
print (celsius,'celsius degress are equivalent to',kelvin,'kelvin degrees')

#Exercise #5

1
'hello'
'What'+'is'+'my'+'name'
30/5

#Exercise #6
    #yes. returned may be printed but it is not required
    #When we assign a variable its value is returned without printing, to see we have to call the variable
    #We may assign a value to x (e.g. x=5), but nothing will happen, we have to call x to see its value printed..